import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import layout from '../constants/Layout';

export default class TourDetails extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('item').name,
      headerStyle: {
        backgroundColor: 'orange',
      },
      headerTintColor: '#fff',
    };
  };

  

  render() {
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={{uri: item.image}}></Image>
        <ScrollView style={styles.body}>
          <View style={styles.title}>
            <Text style={{fontSize: 18, fontWeight: 'bold'}}> {item.name} </Text>
          </View>
          <View>
            <Text>Loreim</Text>
            <Text>Loreim</Text>
            <Text>Loreim</Text>
            <Text>Loreim</Text>
            <Text>Loreim</Text>
            <Text>Loreim</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  image: {
    width: layout.window.width,
    height: layout.window.height * 0.3,
  },
  body: {
    flex: 1,
    flexDirection: 'column',
    margin: 12,
  },
  title: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 12
  },
});