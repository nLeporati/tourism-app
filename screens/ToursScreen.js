import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  View,
  FlatList,
} from 'react-native';
import { MapView } from 'expo';
import { Ionicons, Entypo, MaterialIcons } from '@expo/vector-icons';

import { MonoText } from '../components/StyledText';

const { height, width } = Dimensions.get('screen');

const image = 'http://lorempixel.com/960/634/city';

const data = [
  {key: '0', name: 'Raul Lopez', type: 'Car', image: image},
  {key: '1', name: 'Miguel Perez', type: 'Walking', image: image},
  {key: '2', name: 'Rocio Mandela', type: 'Car', image: image},
  {key: '3', name: 'Nicolás Felipe', type: 'Walking', image: image},
  {key: '4', name: 'Andrés Guajardo', type: 'Walking', image: image},
  {key: '5', name: 'Jennifer Ainston', type: 'Walking', image: image},
  {key: '6', name: 'Brad Pitt', type: 'Car', image: image},
  {key: '7', name: 'Mark Paak', type: 'Walking', image: image},
];

export default class ToursScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      sizeStyle: {height: height * 0.3},
      isOpened: true,
    }
  }

  _toggleToursList() {
    this.setState(state => { return {isOpened: !state.isOpened}});
  }

  _navToDetails(item) {
    this.props.navigation.navigate('Details', {item: item});
  }

  static navigationOptions = {
    header: null,
  };

  renderHeader() {
    return (
      <View style={styles.header}>
        <View style={styles.headerContent}>
          <MaterialIcons name="menu" size={32} color="white" ></MaterialIcons>
          <Text style={{fontSize: 24, color: 'white'}}>Santiago, Chile</Text>
          <Entypo name="location-pin" size={32} color="white" />
        </View>
      </View>
    )
  }

  renderMap() {
    return (
      <View style={styles.map}>
        <MapView
          style={{ flex: 1 }}
          initialRegion={this.state.region}
          showsUserLocation={true}
          rotateEnabled={false}
        />
      </View>
    );
  }

  renderList() {
    return (
        <View style={[styles.tour]}>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <TouchableOpacity onPress={() => this._toggleToursList() }>
              <View style={styles.tourHeader}>
                { this.state.isOpened && <Ionicons name="ios-arrow-down" size={32} ></Ionicons>}
                { !this.state.isOpened && <Ionicons name="ios-arrow-up" size={32} ></Ionicons>}
              </View>
            </TouchableOpacity>
            { this.state.isOpened && <FlatList
              data={data}
              renderItem={({item}) =>
                <TouchableOpacity onPress={() => this._navToDetails(item)}>
                  <View style={styles.tours}>
                    <Text> {item.name} </Text>
                    <Text> {item.type} <Ionicons name="md-walk" size={16} /> </Text>
                  </View>
                </TouchableOpacity>
              }
            />}
          </View>
        </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderHeader()}

        {this.renderMap()}
        {this.renderList()}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  header: {
    zIndex: 9,
    backgroundColor: 'orange',
    position: 'absolute',
    top: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: height * 0.1,
    width: width,
    paddingHorizontal: 14,
    paddingVertical: 14,
    borderBottomLeftRadius:12,
    borderBottomRightRadius: 12,
    shadowColor: '#000000',
    shadowOpacity: 0.4,
    shadowRadius: 8,
    elevation: 4,
  },
  headerContent: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'baseline',
    justifyContent: 'space-between',
  },
  map: {
    flex: 1,
  },
  tour: {
    zIndex: 9,
    backgroundColor: '#FFF',
    position: 'absolute',
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden',
    bottom: 0,
    maxHeight: height * 0.3,
    width: width,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,

  },
  tourHeader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    padding: 2,
    margin: 2,
  },
  tours: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'baseline',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    paddingVertical: 21,
    fontSize: 16,
  },
});
