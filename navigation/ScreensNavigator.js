import React from 'react';
import { createStackNavigator } from 'react-navigation';

import ToursScreen from '../screens/ToursScreen';
import SettingsScreen from '../screens/SettingsScreen';
import TourDetails from '../screens/TourDetails';

export default createStackNavigator({
  Tours: ToursScreen,
  Details: TourDetails,
  Settings: SettingsScreen,
});
